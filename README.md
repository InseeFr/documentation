# Traitement des demandes de mise en place de dépôts externes et services intermédiaires de déploiement

## Principe
Organiser l’environnement de travail des développeurs pour la maintenance et le développement courant, notamment du fait que le taux d’équipement en nomade des développeurs n’est pas de 100 %. Par contre il est nécessaire pour effectuer des mises en production qu’au moins 1 développeur ait un poste nomade Insee.

On vise une centralisation et traçabilité des démarches.
A cet égard, tous les projets seront regroupés au sein du Groupe InseeFr sur Gitlab.com (co-administré par Denis Marchal, Franck Cotton et Juliette Fourcot).
Par défaut, un sous-groupe privé est construit pour chaque projet et tous les travaux devront être rapatriés en interne à l’Insee à l’issue de la période de confinement.
Les demandes seront gérées au cas par cas, et d’abord un projet pilote par SNDI. Ces projets pourront ensuite assister les autres équipes.

Toute autre solution d’externalisation est à proscrire.

## Prérequis
* **Création d’un compte GitHub**
    * Ne pas créer de compte sur gitlab.com, mais créer un compte sur github.com
    * Renseigner dans le profil sur GitHub le nom, le prénom et l’adresse mail professionnelle. Noms et prénoms doivent être en typographie riche (Par exemple François Dupré)
    * Effectuer une première connexion à gitlab.com en utilisant son compte GitHub
    * Prévenir ensuite les administrateurs GitHub/GitLab pour que le compte soit inclus dans l’organisation InseeFr sur GitHub.

* **Nettoyage du dépôt actuel** avec (à choisir ce qui est pertinent) :
    * [Gitsizer](https://github.com/github/git-sizer) : il permet de faire un scan du dépôt Git et d'identifier par exemple si des gros volumes ont été versionnés par le passé
    * [BFG](https://rtyley.github.io/bfg-repo-cleaner/) : il permet de supprimer de gros fichiers et des mots de passe, y compris dans l'historique

* **Vérification que les jeux de test ne contiennent pas de données personnelles ou sensibles du point de vue métier**
    * et plus généralement, vérification que le dépôt ne contient pas de données sensibles du point de vue technique (URL internes, configurations de sécurité, etc.).

## Possibilités envisagées
* migration du dépôt git seul : 
    * avantages : les identités sont correctement gérées, il n’y a pas de perte, les migrations aller et retour sont faciles ;
    * inconvénients : pas de récupération des méta-informations (issues, merge requests, gestion des travaux), pas d’environnement de déploiement
* migration du projet gitlab :
    * avantages : intègre la migration du dépôt git, permet de récupérer aussi la liste des issues, des gestions de tâches ; relativement léger
    * inconvénient : petite perte d’information sur les identités des issues, discussions, à la migration aller comme à la migration retour
* reconfiguration du pipeline d’intégration continue :
    * avantages : permet de conserver au sein du projet l’automatisation des tests unitaires et de la compilation ; relativement léger
    * inconvénient : pas toutes les fonctionnalités internes (sonar par exemple), et opération inverse à prévoir
* mise en place d’environnements de déploiement de test cloud : 
    * avantages : permet de disposer d’environnement de recette externalisés (donc accessibles à des équipes métier ne disposant pas de nomade), adoption des pratiques cloud native / outils devops, utile pour une mutualisation / publication en open source car garantit une réutilisation dans un contexte non Insee
    * inconvénient : très coûteux en temps en particulier s’il faut revoir l’architecture de l’application, notamment dans ses versions de développement (BDD, authentification, annuaire) ; service payant ; opération inverse comparativement peu coûteuse car un cluster Kubernetes est disponible sur la plateforme innovation

## Organisation
* Adresser les demandes à : via le chef de service développement à Mylène Chaleix (priorise), copie Juliette Fourcot, Franck Cotton, Denis Marchal ;
* Mylène Chaleix détermine l’ordre de traitement et informe la Diit et la Daap
* Mise en œuvre : les équipes avec si besoin l'aide de la Diit, Daap
* Le responsable applicatif a le rôle de maintainer (ne peut pas changer la visibilité du projet) (https://gitlab.com/help/user/permissions) ni transférer le projet sur un autre espace (l’action symétrique de retour devra être envisagée de la même manière)
* Aucun support une fois les opérations réalisées ; l’opération inverse devra être faite après le terme du confinement


